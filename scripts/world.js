export default class World {
  constructor(canvas) {
    this.canvas = canvas;
    this.drawingContext = canvas.getContext('2d');
    this.subscribers = [];
  }

  subscribe(fn) {
    this.subscribers.push(fn);
    return this;
  }

  start() {
    if (this.started) { return this; }
    this.started = true;

    const self = this;
    (function fn(){
      if (!self.started) { return; }
      self.draw(self.drawingContext);
      requestAnimationFrame(fn);
    })();

    return this;
  }

  stop() {
    this.started = false;
    return this;
  }

  draw() {
    for (let fn of this.subscribers) {
      fn(this.drawingContext, this.canvas);
    }
  }

}
