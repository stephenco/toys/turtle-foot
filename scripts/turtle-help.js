export default class TurtleHelp {
  constructor(typeName, instanceName) {
    this.type = typeName || "TurtleFoot";
    this.name = instanceName || "turtle";
  }

  getHelp(command) {
    let lines = (() => {
      switch((command || '').toLowerCase()) {

        case 'home':
        case 'go home':
        return [
          "HOME tells me to move back to my starting place at the center of the drawing area.",
          "",
          "Here's an example of how you might use my home() function.",
          "",
          this.name + ".home();"
        ];

        case 'clear':
        case 'erase':
        return [
          "CLEAR tells me to erase everything I've drawn so far.",
          "",
          "Here's an example of how you might use my clear() function.",
          "",
          this.name + ".clear();"
        ];

        case 'reset':
        case 'start over':
        return [
          "RESET tells me to erase everything and go back to where I started. It's the",
          "same as using HOME and CLEAR together.",
          "",
          "Here's an example of how you might use my reset() function.",
          "",
          this.name + ".reset();"
        ];

        case 'go':
        case 'start':
        case 'run':
        return [
          "GO tells me to start working on all of the commands you've given me so far.",
          "I remember what you've told me but I won't do anything until you tell me to GO.",
          "",
          "Here's an example of how you might use my go() function.",
          "",
          this.name + ".go();",
          "",
          "If you want me to draw some lines and then GO, you might try something like this.",
          "",
          this.name + ".move( 100 );",
          this.name + ".turn( 120 );",
          this.name + ".move( 100 );",
          this.name + ".turn( 120 );",
          this.name + ".move( 100 );",
          this.name + ".turn( 120 );",
          this.name + ".go();",
          "",
          "Try it out!"
        ];

        case 'alwaysgo':
        case 'always go':
        case 'always':
        return [
          "ALWAYS GO tells me if you want me to do things right away when you say them or",
          "wait until you tell me it's okay to GO. I start out by just doing what you say",
          "when you say it, but your teacher might have you tell me to wait, because even",
          "though that seems harder, it makes you think differently and helps you learn better.",
          "",
          "Here's an example of how you can use my alwaysGo(...) function to make sure I wait",
          "when you tell me things.",
          "",
          this.name + ".alwaysGo( false );",
          "",
          "You can use true intead of false in that example to switch me back to working",
          "right away if you want to, but you can actually do more things with it turned off."
        ];

        case 'location':
        case 'where':
        return [
          "LOCATION tells me that you want to know where I'll be at after running the rest",
          "of the commands that you've given me so far. You can assign my response to a variable",
          "so that you can use it again later.",
          "",
          "Here's an example of how you might use my location() function.",
          "",
          "var location = " + this.name + ".location();"
        ];

        case 'pen':
        return [
          "PEN is a function that you can use to tell me that you want to change how I'm drawing.",
          "When you use the pen(...) function, you have to tell me what you want to b passing some",
          "parameters.",
          "",
          "The first one is whether I should be using my pen to draw right now. You can",
          "say true or false, and I'll use that just like if you told me PEN UP or PEN DOWN.",
          "",
          "The second one is the color you want me to use. You can say \"blue\" or \"green\", or",
          "tell me some other color. You can even use more advanced color notations when you learn",
          "about those.",
          "",
          "The third parameter tells me how wide of a line you want me to use when I draw things.",
          "You can give me any number, but something between 1 and 25 is probably best.",
          "",
          "Here's an example of how you might use my pen(...) function.",
          "",
          this.name + ".pen( true, \"red\", 5 );"
        ];

        case 'penup':
        case 'pen up':
        case 'up':
        return [
          "PEN UP tells me that you want me to pick my pen pen up off the drawing area and NOT draw",
          "lines every time I move from now on.",
          "",
          "Here's an example of how you might use my penUp() function.",
          "",
          "turtle.penUp();",
        ];

        case 'pendown':
        case 'pen down':
        case 'down':
        return [
          "PEN DOWN tells me that you want me to put my pen down on the drawing area and draw lines",
          "every time I move from now on.",
          "",
          "Here's an example of how you might use my penDown() function.",
          "",
          "turtle.penDown();",
        ];

        case 'pencolor':
        case 'pen color':
        case 'color':
        return [
          "PEN COLOR lets me know what color you want me to draw lines from now on.",
          "When you use PEN COLOR you have also tell me what color to use. You can say \"blue\"",
          "or \"green\", or tell me some other color. You can even use more advanced color",
          "notations when you learn about those.",,
          "",
          "Here's an example of how you might use my penColor(...) function.",
          "",
          "turtle.penColor( \"yellow\" );",
        ];

        case 'penwidth':
        case 'pen width':
        case 'width':
        return [
          "PEN WIDTH tells me how wide of a line you want me to use when I draw things.",
          "When you use PEN WIDTH you have to also give me a number for how wide my lines should be.",
          "You can give me any number, but something between 1 and 25 is probably best.",
          "",
          "Here's an example of how you might use my penWidth(...) function.",
          "",
          this.name + ".penWidth( 5 );",
        ];

        case 'turn':
        case 'rotate':
        return [
          "TURN tells me that you want me to change where I'm looking by turning myself some number of",
          "degrees. Remember that there are 360 degrees in a circle, and that for me, 0 is straight ahead.",
          "90 straight to my left, and 180 would be straight behind me. If you want me to turn to my left,",
          "you should give me a positive number. That makes me turn counter-clockwise. If you want me to turn",
          "to my right though, you should give me a negative number and I'll turn clockwise. For instance, if",
          "you told me to turn -90 degrees, I would turn to look straight over my shoulder to my right.",
          "",
          "Here's an example of how you might use my turn(...) function.",
          "",
          this.name + ".turn( 45 );"
        ];

        case 'rotation':
        return [
          "ROTATION tells me to look in a specific direction. It's different than TURN because you give",
          "me the exact direction that you want me to look instead of telling me to turn myself a little",
          "bit at a time. You can use this to tell me to point in a certain way, ignoring where I'm looking",
          "right now.",
          "",
          "When you use my ROTATION function, think about 0 pointing to the right of the drawing area,",
          "90 pointing up, 180 pointing to the left side, and 270 pointing down.",
          "",
          "Here's an example of how you might use my rotation(...) function.",
          "",
          this.name + ".rotate(45);"
        ];

        case 'move':
        return [
          "MOVE tells me that you want me to go forward for whatever distance you say. Remember that",
          "I'll move in the direction that I'm looking, so if you want me to move in different directions",
          "you'll have to tell me to TURN first.",
          "",
          "Also, don't forget to tell me to GO when you want to see me move and draw lines. I won't actually",
          "do anything until you tell me to GO.",
          "",
          "Here's an example of how you might use my move(...) function.",
          "",
          this.name + ".move( 200 );"
        ];

        case 'jump':
        return [
          "JUMP tells me that you want me to move straight to some place in the drawing area that you tell",
          "me. That means you have to give me both a left/right number (which I call X) and an up/down number",
          "(that I call Y). Remember that I can always tell you where I am if you ask me by using my LOCATION",
          "function. You can use the answer that I give you together with JUMP to have me go back to where I",
          "was after doing a bunch of other things, if you want to.",
          "",
          "Here's an example of how you might use my jump(...) function.",
          "",
          this.name + ".jump( 200, 100 );",
          "",
          "And here's one using LOCATION also.",
          "",
          "var startingLocation = " + this.name + ".location();",
          "    ... do a bunch of other stuff ...",
          this.name + ".jump( startingLocation );"
        ];

        case 'random':
        return [
          "RANDOM tells me to make a random number between zero and whatever number you give me.",
          "You can assign my answer to a variable that you can use later, or you can use it together with",
          "one of my other functions to do funny things.",
          "",
          "Here's an example of how you might use my random(...) function to make a variable.",
          "",
          "var value = " + this.name + ".random( 10 );",
          "",
          "And here's how you might use it to get me to turn in a random counter-clockwise direction.",
          "",
          this.name + ".turn( " + this.name + ".random( 90 ) );"
        ];

        case 'wait':
        case 'stop':
        case 'pause':
        return [
          "WAIT tells me to pause and wait for a certain amount of time before starting to do other stuff.",
          "You can use this to make me draw slower so that you can see it one step at a time, or so that",
          "it looks like I'm animating my path as I GO. Remember that I still won't actually do anyting",
          "until you use my GO function.",
          "",
          "You have to give me a number to tell me how long to wait. Bigger numbers tell me to wait for",
          "longer, but don't make it too big or you might be waiting for me to WAIT for longer than you want!",
          "I can't do anyting else while I'm waiting, so you need to be careful. Something between 1 and 5",
          "is usually good to start until you practice and get a better sense for what different numbers do.",
          "",
          "Here's an example of how you might use my wait(...) function.",
          "",
          this.name + ".wait( 2 );"
        ];

        case 'loop':
        case 'repeat':
        return [
          "REPEAT tells me to do something over and over a certain number of times. You have to first tell me",
          "how many times you want me to repeat something, and then tell me what the something is that you want",
          "me to repeat. You can do this by telling me about some other function that you make that gives me other",
          "commands. I will run that function however many times you tell me.",
          "",
          "You can use my repeat(...) function to do all kinds of tricky things after a little bit of practice.",
          "At first it might be hard to understand because of all of the extra ( ) and { } and ; things, but that's",
          "real JavaScript code and you can do it. Pretty soon, if you practice, it won't be hard at all!",
          "",
          "Here's an example of how you might use my repeat(...) function to draw a square.",
          "",
          this.name + ".repeat(4, function() {",
          "    " + this.name + ".turn(90);",
          "    " + this.name + ".move(100);",
          "});",
          "",
          "And by the way, did you remember that a circle has 360 degrees in it? You can use that thing that you know",
          "to draw any kind of a regular shape that you want! For example, a octagon has 8 sides, like a stop sign, so",
          "here's how you you might use my repeat(...) function to draw an octagon instead of a square. The trick is",
          "just to divide 360 by the number of sides you want to draw!",
          "",
          this.name + ".repeat(8, function() {",
          "    " + this.name + ".turn( 360 / 8 );",
          "    " + this.name + ".move(100);",
          "});",
        ];

        case 'welcome':
        case 'hi':
        return [
          "WELCOME tells me that you want me to draw my welcome screen again. That's the one that says \"Hi\" when",
          "I first start up. When you do this I'll clear the rest of the drawing area first though, so be careful.",
          "",
          "Here's an example of how you can use my welcome() function.",
          "",
          this.name + ".welcome();"
        ];

        case '':
        case 'help':
        return [
          "Hi, I'm " + this.type + "! But you can just call me " + this.name + ".",
          "",
          "You can tell me to do things and I'll do my best to remember what you say. I can",
          "draw lines and shapes when you ask me to move and turn, or you can tell me if I",
          "should always wait until you say to GO.",
          "",
          "I only know a few words, but you can learn to do a lot with me if you try and",
          "practice for a while. I'm built with a language called JavaScript that runs in",
          "your web browser, and when you talk to me you're learning how to program!",
          "",
          "Here's the list of things you can tell me to do. These are the words that I know.",
          "",
          "turn, move, go, alwaysGo, wait, clear, reset, penUp, penDown, penColor, repeat,",
          "rotation, random, location, jump, welcome, and help",
          "",
          "Each word is called a \"function\" and you can combine my functions to do all kinds",
          "of fun stuff. I can even help you out with each of my functions if you want to ask",
          "me about them. You just have to use my HELP function if you want to know more. For",
          "example, here's how you can ask for help about GO, if you want.",
          "",
          this.name + ".help(\"go\");",
          "",
          "Maybe you could try doing that now!",
          "",
          "Remember that you can do that for any of the words that I know. Just make sure you",
          "put the name in quotes, like \"this\", or I might get a little bit confused."
        ];

        default:
        return [
          "I'm sorry, I don't think I know about that function."
        ]
      }
    })();
    lines.unshift("\n");
    lines.push("\nYou can always ask me for more help by typing " + this.name + ".help(); or learn more" +
      "\nabout any of my functions by typing " + this.name + ".help(\"function name\"); Be sure to" +
      "\nuse a real function name for the \"function name\" part, though!\n\n\n");

    // const ms = 20;
    // for (let x = 0; x < lines.length; x++) {
    //   setTimeout(() => console.info(lines[x]), x * ms);
    // }

    console.info(lines.join('\n'));

    return this;
  }
}
