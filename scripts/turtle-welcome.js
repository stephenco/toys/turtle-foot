export default class TurtleWelcome {
  constructor(turtle) {
    this.turtle = turtle;
  }

  draw(fnAfter) {
    const t = this.turtle;
    const w = 2;

    function splat() {
      t.repeat(100, function() {
        var r, g, b;
        r = g = b = 255 - t.random(60);
        t.home().penWidth(t.random(10));
        t.penColor('rgb(' + r + ', ' + g + ', ' + b +')');
        t.repeat(400, function() {
          t.turn(t.random(100) - 50).move(t.random(10));
        });
      });
    }

    function puff(sides, size) {
      t.repeat(sides, function() {
          var loc = t.location();
          t.penDown();
          t.turn(360/sides);
          t.move(size);
          t.wait(w);
          t.penUp();
          t.jump(loc);
      });
    }

    function lettering(width, color) {
      t.home().rotation(0).penWidth(width).penColor(color).turn(5).wait(10);

      t.penUp().move(-60).penDown();
      t.move(-10).wait(w)
        .turn(90).move(100).wait(w)
        .turn(-90).move(10).wait(w)
        .turn(-90).move(45).wait(w)
        .turn(90).move(45).wait(w)
        .turn(90).move(45).wait(w)
        .turn(-90).move(10).wait(w)
        .turn(-90).move(100).wait(w)
        .turn(-90).move(10).wait(w)
        .turn(-90).move(45).wait(w)
        .turn(90).move(45).wait(w)
        .turn(90).move(45).wait(w)
        .turn(90);

      t.penUp().move(90).penDown();
      t.move(-10).turn(90).wait(w)
        .move(50).turn(-90).wait(w)
        .move(10).turn(-90).wait(w)
        .move(50).turn(90).wait(w);

      t.penUp().turn(-90).move(-80).turn(-90).move(5);
      puff(7, 10);
      t.penDown().penColor('red').turn(-5);

      t.turn(180);
      t.go();
    }

    setTimeout(() => {
      for (let i = 0; i < 5; i++) {
        splat();
      }
      lettering(10, '#fff');
      lettering(6, '#9cf')
      lettering(2, '#69d');
      t.stack.push(fnAfter);
    }, 1);
  }
}
