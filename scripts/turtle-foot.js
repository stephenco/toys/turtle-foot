import TurtleHelp from './turtle-help';
import TurtleWelcome from './turtle-welcome';

export default class TurtleFoot {
  constructor(name, world, fnCallback) {
    const self = this;

    this.notify = (name) => {
      if (self.allowNotify) {
        (fnCallback || function(){})(name, {
          x: +(self.settings.location.x || 0),
          y: +(self.settings.location.y || 0),
          rotation: +(self.settings.location.rotation || 0)
        });
      }
      return self;
    };

    this.math = {
      radiansMultiplier: Math.PI / 180,
      baseRotation: 90
    };

    this.world = world.subscribe((context, canvas) => {
      if (!self.stack.length && !self.settings.paused) { self.settings.paused = true; }
      if (self.settings.paused || self.settings.stopped) { return; }

      var remaining = [];
      for (let fn of self.stack) {
        if (!fn) { continue; }
        if (self.settings.paused || self.settings.stopped) { remaining.push(fn); }
        else { fn(context); }
      }

      self.stack = remaining;
    });

    const help = new TurtleHelp(this.constructor.name, name);
    const commands = [
      'reset', 'go', 'alwaysGo', 'home', 'clear', 'location',
      'pen', 'penUp', 'penDown', 'penColor', 'penWidth',
      'turn', 'rotate', 'rotation', 'move', 'jump', 'random',
      'loop', 'repeat', 'wait', 'welcome'
    ];
    for (let cmd of commands) {
      this[cmd].help = () => help.getHelp(cmd);
    }
    this.help = (...args) => help.getHelp(...args);

    this.welcome();
  }

  welcome() {
    const self = this;
    delete this.allowNotify;
    this.reset();
    new TurtleWelcome(this).draw(() => {
      self.allowNotify = true
      self.notify('ready');
      self.alwaysGo(true);
    });
    return this;
  }

  go() {
    this.settings.paused = false;

    return this;
  }

  alwaysGo(on) {
    this.settings.alwaysGo = !!on;
    return this.settings.alwaysGo ? this.go() : this;
  }

  home() {
    const self = this;
    let homeX = this.world.canvas.width * 0.5;
    let homeY = this.world.canvas.height * 0.5
    this.settings.location.x = homeX;
    this.settings.location.y = homeY;
    this.stack.push(c => {
      self.notify('home');
      c.moveTo(homeX, homeY);
    });
    return this.settings.alwaysGo ? this.go() : this;
  }

  reset() {
    const self = this;
    const startX = this.world.canvas.width * 0.5;
    const startY = this.world.canvas.height * 0.5
    const color = 'red';
    const width = 2;
    const alwaysGo = (this.settings || {}).alwaysGo;
    this.settings = {
      alwaysGo: alwaysGo,
      paused: true,
      pen: { down: true, color: color, width: width },
      location: { x: startX, y: startY, rotation: 0 }
    };
    this.stack = [ c => {
      self.notify('reset');
      c.moveTo(startX, startY);
      c.strokeStyle = color;
      c.lineWidth = width;
    }];
    return this.clear();
  }

  location() {
    return {
      x: this.settings.location.x,
      y: this.settings.location.y
    };
  }

  clear() {
    const self = this;
    this.stack.push(c => c.clearRect(0, 0, self.world.canvas.width, self.world.canvas.height));
    return this.settings.alwaysGo ? this.go() : this;
  }

  wait(time) {
    if (!time) { return; }
    const self = this;
    this.stack.push(c => {
      self.settings.paused = true;
      setTimeout(() => {
        self.settings.paused = false;
      }, time * 10)
    });
    return this.settings.alwaysGo ? this.go() : this;
  }

  stop() {
    const self = this;
    this.stack.push(c => self.settings.stopped = true);
    return this;
  }

  start() {
    const self = this;
    this.stack.push(c => self.settings.stopped = false);
    return this;
  }

  pen(down, color, width) {
    this.settings.pen.down = !!down;
    if (color) {
      this.settings.pen.color = color;
      this.stack.push(c => c.strokeStyle = color);
    }
    if (width) {
      this.settings.pen.width = width;
      this.stack.push(c => c.lineWidth = width);
    }
    return this;
  }

  penDown() { return this.pen(true); }
  penUp() { return this.pen(false); }
  penColor(c) { return this.pen(this.settings.pen.down, c); }
  penWidth(w) { return this.pen(this.settings.pen.down, this.settings.pen.color, w); }

  turn(degrees) {
    if (degrees) {
      const start = this.settings.location.rotation || 0;
      const self = this;
      let end = start + degrees;
      while (end > 359) { end -= 360; }
      while (end < 0) { end += 360; }
      this.settings.location.rotation = end;
      this.stack.push(c => {
        self.notify('turn');
      });
    }
    return this.settings.alwaysGo ? this.go() : this;
  }

  rotate(...args) { return this.turn(...args); }

  rotation(degrees) {
    this.settings.location.rotation = degrees;
    return this.settings.alwaysGo ? this.go() : this;
  }

  move(distance) {
    if (distance) {
      const self = this;
      function radians(d) {
        return d * self.math.radiansMultiplier;
      }
      let a = radians((this.settings.location.rotation || 0) + this.math.baseRotation);
      let x = this.settings.location.x;
      let y = this.settings.location.y;
      let dx = Math.sin(a) * distance;
      let dy = Math.cos(a) * distance;
      this.settings.location.x = x + dx;
      this.settings.location.y = y + dy;
      if (this.settings.pen.down) {
        this.stack.push(c => {
          self.notify('moved');
          c.beginPath();
          c.moveTo(x, y);
          c.lineTo(x + dx, y + dy);
          c.stroke();
        });
      } else {
        this.stack.push(c => {
          self.notify('moved');
          c.moveTo(x + dx, y + dy);
        });
      }
    }
    return this.settings.alwaysGo ? this.go() : this;
  }

  jump(point) {
    if (point) {
      if (this.settings.pen.down) {
        this.stack.push(c => {
          c.beginPath();
          c.moveTo(this.settings.location.x, this.settings.location.y);
          c.lineTo(point.x || 0, point.y || 0);
          c.stroke();
        });
      } else {
        this.stack.push(c => c.moveTo(point.x || 0, point.y || 0));
      }
      this.settings.location.x = point.x || 0;
      this.settings.location.y = point.y || 0;
    }
    return this.settings.alwaysGo ? this.go() : this;
  }

  random(x) {
    return Math.random() * (x || 1);
  }

  loop(max, fn) {
    if (max && max > 0) {
      for (let x = 0; x < max; x++) {
        fn();
      }
    }
    return this.settings.alwaysGo ? this.go() : this;
  }

  repeat(...args) { return this.loop(...args); }

}
