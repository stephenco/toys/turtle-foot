import World from './world.js'
import TurtleFoot from './turtle-foot.js'
import TurtleOverlay from './turtle-overlay';

((wc, oc) => {

  window.turtle = new TurtleFoot("turtle",
    new World(wc).start(),
    new TurtleOverlay(oc).start().handler);

  window.turtle.help();

})(
  document.getElementById("world-canvas"),
  document.getElementById("overlay-canvas")
);
