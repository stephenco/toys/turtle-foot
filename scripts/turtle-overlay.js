export default class TurtleOverlay {
  constructor(canvas) {
    this.canvas = canvas;
    this.drawingContext = canvas.getContext('2d');

    const self = this;
    this.handler = (name, loc) => {
      self.location = loc;
    };
  }

  start() {
    if (this.started) { return this; }
    this.started = true;

    const self = this;
    (function fn(){
      if (!self.started) { return; }
      self.draw(self.location, self.drawingContext, self.canvas);
      requestAnimationFrame(fn);
    })();

    return this;
  }

  stop() {
    this.started = false;
    return this;
  }

  draw(loc, c, cc) {
    const prev = this.prev || {};
    if (!loc || ( loc.x == prev.x && loc.y == prev.y && loc.rotation == prev.rotation )) {
      // only update on valid input and if something changes
      return;
    }

    this.prev = prev;

    function radians(deg) {
      return deg * Math.PI / 180;
    }

    c.save();
    c.clearRect(0, 0, cc.width, cc.height);
    c.translate(loc.x, loc.y);
    c.rotate(radians(loc.rotation) * -1);

    (function drawTurtle(x, y) {
      c.fillStyle = "#fff";
      c.strokeStyle = "#000";
      c.beginPath();
      c.moveTo(x, y);
      c.lineTo(x + 20, y);
      c.stroke();
      c.beginPath();
      c.arc(x, y, 5, 0, 2*Math.PI);
      c.fill();
      c.stroke();
      c.fillStyle = "#68f";
      c.beginPath();
      c.arc(x, y, 2, 0, 2*Math.PI);
      c.fill();
    })(0, 0);

    c.restore();

  }

}
